# README #
This is the repository with generated results for the Master Thesis "Entdecken von wiederkehrenden Zeichenketten in Szenarien bei Java Open Source Software zur Unterstützung der Lokalisation von Komponenten".
#############
It has the following structure:
* Calltraces_Database: Contains the MySQL Dumps of the Calltraces made with AspectJ, where the "calltraces_mysql_dump_cleaned.zip" are the Traces with already removed background noise.
* Cleaned: Results from cleaned Traces
* ExampleDistributedResult/help_nogap_nosyns: One (compressed) plain result of a distributed analysis collected on the master server. They are huge so only one example to see the format is provided
* NoGap-NoSyns: Results without using a gap or synonyms
* NoGap-WithSyns: Results without using a gap but with synonyms from "WordNet"
* Quer/quer_argouml_dateierstellen-argouml_suchen_cleaned_gap: One example of what happens if two different scenarios are compared from the same application
* WithGap: Results with using a gap, mostly with a gap=10, but one example was done with gap=3
* Wordcount: Counted occurence of words, e.g. to generate tac clouds with "http://www.wordle.net/"
#################

Some notes:
Not all anlysis are done with all applications. "fullresults.txt" and "tokenresults.txt" are the results directly from the anlysis. "calculated.txt.xls" is further aggregated and most probably the more interesting part. "joeffice_results.xls" is the result of the comparison of PyCharm/ArgoUML with Joeffice.

For more information please consult the corresponding Master Thesis.